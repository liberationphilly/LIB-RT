const schedule  = require('node-schedule')
const matrixSdk = require('matrix-js-sdk')

/*******************************************************************************
* Matrix connect
*******************************************************************************/

const mainRoomId = '!iByXnwwXsNDSSMJNJu:matrix.org'
const botId      = '@librt:matrix.org'

var matrixClient = matrixSdk.createClient({
    baseUrl: 'https://matrix.org',
    accessToken: process.env['MATRIX_ACCESS_TOKEN'],
    userId: botId
})

/*******************************************************************************
* Core functions
*******************************************************************************/

// Messages any Matrix room
function sendToRoom(roomId, message) {
  // 2 second delay in messages so it shows LIB-RT "typing"
  const typingTime = 2000
  matrixClient.sendTyping(roomId, true, typingTime, function() {
    setTimeout(function() {
      matrixClient.sendMessage(roomId, {
        "msgtype": "m.text",
        "body": message
      })
      matrixClient.sendTyping(roomId, false)
    }, typingTime)
  })
}

// Messages the main room
function send(message) {
  sendToRoom(mainRoomId, message)
}

// Joins any Matrix room
function joinRoom(roomId) {
  matrixClient.joinRoom(roomId).done(function() {
    sendToRoom(roomId, "Animal liberation now!")
  })
}

// Provides a callback to handle incoming messages
function onMessage(callback) {
  matrixClient.once('sync', function(state, prevState) {
    if (state === 'PREPARED') {
      matrixClient.on("Room.timeline", function(event, room, toStartOfTimeline) {
        if (toStartOfTimeline || event.getSender() === botId) {
          return // Ignore LIB-RT's own messages
        }
        if (event.getType() !== "m.room.message") {
          return // Only respond to normal messages
        }
        callback(room.roomId, event.getContent().body)
      })
    }
  })
}

// Provides a callback to handle room invitations
function onInvited(callback) {
  matrixClient.on("RoomMember.membership", function(event, member) {
    if (member.membership === "invite" && member.userId === botId) {
      callback(member.roomId)
    }
  })
}

// Schedules reminders
function remind(frequency, message) {
  schedule.scheduleJob(frequency, function() {
    send(message)
  })
}

// Starts LIB-RT
function start() {
  matrixClient.startClient()
}

/*******************************************************************************
* Export as a module
*******************************************************************************/

module.exports = {
  mainRoomId: mainRoomId,
  botId: botId,

  onMessage: onMessage,
  remind: remind,
  send: send,
  sendToRoom: sendToRoom,
  onInvited: onInvited,
  joinRoom: joinRoom,
  start: start
}
