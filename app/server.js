/*
  ██╗     ██╗██████╗       ██████╗ ████████╗
  ██║     ██║██╔══██╗      ██╔══██╗╚══██╔══╝
  ██║     ██║██████╔╝█████╗██████╔╝   ██║
  ██║     ██║██╔══██╗╚════╝██╔══██╗   ██║
  ███████╗██║██████╔╝      ██║  ██║   ██║
  ╚══════╝╚═╝╚═════╝       ╚═╝  ╚═╝   ╚═╝
*/

const librt      = require("./librt")
const randomItem = require("random-item")

/*******************************************************************************
* Store some values to shuffle through
*******************************************************************************/

var greetings = [
  "Animal liberation now!",
  "We can achieve animal liberation together.",
  "It's not food, it's violence!",
  "Liiiiiiibeeeratiiioooonnn, foooooor the animalllssss",
  "In 40 years, in less than 40 years...",
  "Carnies are weak, yo."
]

/*******************************************************************************
* LIB-RT main chat logic
*******************************************************************************/

// Handle incoming messages
librt.onMessage(function(roomId, message) {

  // Greetings
  if (message.match(/\bLIB[-]?RT\b/i)) {
    librt.sendToRoom(roomId, randomItem(greetings))
  }

})

// Automatically join rooms when invited
librt.onInvited(function(roomId) {
  librt.joinRoom(roomId)
})

// AWG reminder
const awgReminder = "Tonight at 6:45pm we're having ALL WORKING GROUPS at Uptwinkles! Be there or be square. Visit #liberationphillyawg:matrix.org to discuss."
librt.remind({ "dayOfWeek": 1, "hour": 10, "minute": 00 }, awgReminder)
librt.remind({ "dayOfWeek": 1, "hour": 14, "minute": 00 }, awgReminder)

// Outreach WG reminder
const outreachReminder = "Tonight at 7pm is our OUTREACH WG meeting! Visit #liberationphillyoutreach:matrix.org to join."
librt.remind({ "dayOfWeek": 2, "hour": 10, "minute": 00 }, outreachReminder)
librt.remind({ "dayOfWeek": 2, "hour": 14, "minute": 00 }, outreachReminder)

// Art WG reminder
const artReminder = "Tonight at 7pm is our ART WG meeting! Visit #liberationphillyart:matrix.org to join."
librt.remind({ "dayOfWeek": 3, "hour": 10, "minute": 00 }, artReminder)
librt.remind({ "dayOfWeek": 3, "hour": 14, "minute": 00 }, artReminder)

// Tech WG reminder
const techReminder = "Tonight at 6:30pm is our TECH WG meeting! Visit #liberationphillytech:disroot.org to join."
librt.remind({ "dayOfWeek": 4, "hour": 10, "minute": 00 }, techReminder)
librt.remind({ "dayOfWeek": 4, "hour": 14, "minute": 00 }, techReminder)


/*******************************************************************************
* Start LIB-RT
*******************************************************************************/

librt.start()

// Make LIB-RT announce his presence during a push/reboot so we know he's still working
librt.send("I just woke up from my nap. Have we achieved animal liberation yet?")
