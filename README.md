# LIB-RT

LIB-RT is a Matrix chatbot created for Liberation Philly to keep us updated on working group meetups and events.

![LIB-RT](librt.png)

## License

LIB-RT is licensed under GNU GPL version 3.0. For the full license see the LICENSE file.
